package com.stefan.Company_APP;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    Login login;

    @Autowired
    JobsRepository jobsRepository;

    @RequestMapping("/dashboard")
    public ModelAndView dashboard() {

        ModelAndView modelAndView = new ModelAndView("dashboard");
        return modelAndView;
    }


    @RequestMapping(value = "/showEmployee")
    public ModelAndView showEmployee() {

        ModelAndView modelAndView = new ModelAndView("employeeView");
        modelAndView.addObject("employee", employeeRepository.findAll());

        return modelAndView;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam(value = "id", required = false) Integer userId) {

        if (login.getCurrentAdminId() == 0) {
            return new ModelAndView("redirect:/adminlog.html");
        }

        Employee e;
        if (userId == null) {
            e = new Employee();
        } else {
            e = employeeRepository.findById(userId).get();
        }
        ModelAndView modelAndView = new ModelAndView("index");

        modelAndView.addObject("e", e);

        return modelAndView;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(Employee employee) {

        employeeRepository.save(employee);

        return this.showEmployee();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView add(@RequestParam(value = "id", required = false) Integer userId) {
        Users users;

        if (userId == null) {
            users = new Users();
        } else {
            users = userRepository.findById(userId).get();
        }
        ModelAndView modelAndView = new ModelAndView("users");
        modelAndView.addObject("u", users);


        return modelAndView;
    }

    @RequestMapping("/add")
    public ModelAndView create(Users users) {
        String md5Hex = DigestUtils
                .md5Hex(users.getPassword()).toUpperCase();
        users.setPassword(md5Hex);
        userRepository.save(users);
        return dashboard();
    }


    @RequestMapping(value = "/applyJob", method = RequestMethod.POST)
    public ModelAndView applyJob(@RequestParam(value = "id", required = false) Integer jobId) {
        JobsApplication jobsApplication;

        if (jobId == null) {
            jobsApplication = new JobsApplication();
        } else {
            jobsApplication = jobsRepository.findById(jobId).get();
        }
        ModelAndView modelAndView = new ModelAndView("apply");
        modelAndView.addObject("j", jobsApplication);


        return modelAndView;
    }

    @RequestMapping("/saveJob")
    public ModelAndView saveJob(JobsApplication jobsApplication) {
        jobsRepository.save(jobsApplication);
        return jobs();
    }

    @RequestMapping("/remove")
    public ModelAndView delete(@RequestParam(value = "id") Integer id) {

        if (login.getCurrentAdminId() == 0) {
            return new ModelAndView("redirect:/adminlog.html");
        }
        employeeRepository.deleteById(id);

        return this.showEmployee();
    }

    @RequestMapping("/jobs")
    public ModelAndView jobs() {

        ModelAndView modelAndView = new ModelAndView("job");

        return modelAndView;
    }

    @RequestMapping("/departments")
    public ModelAndView departments() {
        ModelAndView modelAndView = new ModelAndView("departments");
        return modelAndView;
    }

}
