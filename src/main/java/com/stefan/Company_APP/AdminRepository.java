package com.stefan.Company_APP;

import org.springframework.data.repository.CrudRepository;

public interface AdminRepository extends CrudRepository<Admin,Integer>{

    Admin findByUsername(String username);
}
