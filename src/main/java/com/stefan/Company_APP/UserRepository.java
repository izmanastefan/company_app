package com.stefan.Company_APP;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Users, Integer> {

    Users findByUsername(String username);
}
