package com.stefan.Company_APP;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class LoginController {

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    Login login;

    @RequestMapping("/login")
    public ModelAndView login(@RequestParam("username") String username, @RequestParam("password") String password) {
        String md5Hex = DigestUtils
                .md5Hex(password).toUpperCase();

        Admin admin = adminRepository.findByUsername(username);

        if(login.getCurrentUserId()!=0){
            return new ModelAndView("redirect:/loginerror1.html");
        }

        if (admin==null) {
            return new ModelAndView("redirect:/login.html");
        }
        if (!admin.getPassword().equals(md5Hex)) {
            return new ModelAndView("redirect:/login.html");
        }
        login.setCurrentAdminId(admin.getId());
        return new ModelAndView("redirect:/success.html");
    }

    @RequestMapping("/logout")
    public ModelAndView logout() {

        if (login.getCurrentAdminId() == 0 && login.getCurrentUserId() == 0) {
            return new ModelAndView("redirect:/yet.html");
        }

        login.setCurrentAdminId(0);
        login.setCurrentUserId(0);

        return new ModelAndView("redirect:/logout.html");
    }

    @RequestMapping("/loginUser")
    public ModelAndView loginUser(@RequestParam("username") String username, @RequestParam("password") String password) {
        String md5Hex = DigestUtils
                .md5Hex(password).toUpperCase();

        Users users = userRepository.findByUsername(username);

        if(login.getCurrentAdminId()!=0){
            return new ModelAndView("redirect:/loginerror.html");
        }

        if (users == null) {
            return new ModelAndView("redirect:/login1.html");
        }
        if (!users.getPassword().equals(md5Hex)) {
            return new ModelAndView("redirect:/login1.html");
        }
        login.setCurrentUserId(users.getId());
        return new ModelAndView("redirect:/successUser.html");
    }

}
